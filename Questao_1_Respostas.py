#!/usr/bin/env python
# coding: utf-8

# In[1]:


#importando pacotes
import pandas as pd
import numpy as np
import datetime 


# #### Questão 1 - Respostas ####

# In[2]:


df = pd.read_csv('Questão_1_Base.txt', delimiter = "\t")


# In[3]:


df.shape


# In[4]:


df['DT_VENCIMENTO'] = pd.to_datetime(df['DT_VENCIMENTO'])


# In[5]:


df.dtypes


# In[6]:


df['ANO'] = df['DT_VENCIMENTO'].dt.year
df['MES'] = df['DT_VENCIMENTO'].dt.month  


# In[7]:


df.groupby(['MES']).ID_CONTA.count()


# #### 1.1 – Qual o percentual de faturas emitidas por mês no qual os clientes não pagaram a fatura anterior? ####

# In[8]:


#Resposta
total_faturas_emitidas = df.groupby(['MES','ANO']).ID_CONTA.count()
df2 = df[df['DS_ROLAGEM'] == 'FX1']
nao_pagaram_faturas = df2.groupby(['MES']).ID_CONTA.count()
resposta = (nao_pagaram_faturas / total_faturas_emitidas) *100
print(round(resposta))






# #### 1.2 – Tendo como referência todos os clientes que tiveram fatura emitida no mês de setembro, gere uma base para esses clientes com os seguintes calculados:
# #### • Total de faturas emitidas nos últimos 6 meses (sem contar com a fatura de setembro);
# #### • O valor médio de fatura nos últimos 6 meses (sem contar com a fatura de setembro);
# #### • Quantidade de vezes que ele ficou sem pagar a fatura anterior nos últimos 6 meses (sem contar com a fatura de setembro). 

# In[27]:


# Resposta 
df['DT_VENCIMENTO'] = pd.DataFrame(pd.date_range("2019-03-01","2019-08-31"), columns=['DT_VENCIMENTO'])  
df['QTD_FATURAS_ULT_6M'] = df['DS_ROLAGEM']
df['VL_MEDIO_FATURA'] = df['VL_FATURA']
df['QTD_FATURAS_ULT_6M_FX1'] = (df['DS_ROLAGEM'] == 'FX1')
df3 = df.groupby(['ID_CONTA','DS_ROLAGEM','DT_VENCIMENTO']).agg({'QTD_FATURAS_ULT_6M':'count', 'VL_MEDIO_FATURA':'mean', 'QTD_FATURAS_ULT_6M_FX1':'count'})
df3.head()


# In[28]:


df3.tail()


# #### 1.3 – Utilizando como referência a base calculada na questão anterior, identifique qual das 3 variáveis calculadas tem o maior potencial de preditivo em relação a variável DS_ROLAGEM do mês de setembro. ####

# In[30]:


#Resposta
df['DT_VENCIMENTO'] = pd.DataFrame(pd.date_range("2019-09-01","2019-09-30"), columns=['DT_VENCIMENTO'])  
df['QTD_FATURAS_ULT_6M'] = df['DS_ROLAGEM']
df['VL_MEDIO_FATURA'] = df['VL_FATURA']
df['QTD_FATURAS_ULT_6M_FX1'] = (df['DS_ROLAGEM'] == 'FX1')
df3 = df.groupby(['ID_CONTA','DS_ROLAGEM','DT_VENCIMENTO']).agg({'QTD_FATURAS_ULT_6M':'count', 'VL_MEDIO_FATURA':'mean', 'QTD_FATURAS_ULT_6M_FX1':'count'})
df3.head(10)


# In[ ]:


# Aparentemente a variável VL_MEDIO_FATURA tem o maior potencial de preditivo com relação a DS_ROLAGEM


# #### FIM ####

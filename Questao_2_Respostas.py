#!/usr/bin/env python
# coding: utf-8

# In[97]:


#importando pacotes
import pandas as pd
import numpy as np
import datetime
import seaborn as sns
import matplotlib.pyplot as plt


# In[98]:


#Carregando o dataset
df = pd.read_csv('Questao_2_total.csv')


# #### Análise exploratória e limpeza/transformação ####

# In[99]:


df.head()


# In[100]:


df.shape


# In[101]:


df = df.fillna(0)


# In[102]:


df.isnull().sum()


# In[103]:


#Valores unicos em cada variável
for feature in df.columns:
    print(feature,':', len(df[feature].unique()))


# In[104]:


#Variável Target
print("Aderiu ao acordo: ", df['RESPOSTA'].value_counts()[1])
print("Não aderiu ao acordo: ", df['RESPOSTA'].value_counts()[0])
print("Total: ", df['RESPOSTA'].value_counts()[1] + df['RESPOSTA'].value_counts()[0])


# #### Questão 2 - RESPOSTAS ####

# #### 2.1 – Qual o percentual de adesão mensal por faixa de atraso (Histórico)? ####

# In[109]:


### Resposta 

df['aderiu'] = df['RESPOSTA'] == 1
df['total'] = df['ID_CONTA'].value_counts()
df['percentual'] = (df['aderiu'] / df['total']) * 100
df['DT_ACORDO'] = pd.to_datetime(df['DT_ACORDO'])
df['mes'] = df['DT_ACORDO'].dt.month
df['ano'] = df['DT_ACORDO'].dt.year
round(df.groupby(['mes','ano']).percentual.mean(),2)


# #### 2.2 – Qual modelo preditivo você utilizaria para traçar uma estratégia objetivando o aumento da adesão dos acordos? (Descreva a técnica utilizada) ####

# In[ ]:


# Resposta
# Foi utilizado uma técnica de classificação através de Regressão para estimar a probabilidade de aderir ao acordo com base no histórico.


# #### 2.3 Quais indicadores e ferramentas você utilizaria para avaliar a performance/aderência desse modelo? (Descreva os indicadores utilizados) ####

# In[ ]:


# Resposta
# Foi utilizado accuracy_score, confusion_matrix e classification_report para avaliar a qualidade do modelo.


# #### 2.4 – Apresente o modelo desenvolvido utilizando a técnica do item (2.2) e as técnicas de avaliação descritas no item (2.3). ####

# In[110]:


#Correlação para verificar se há multicolinearidade
plt.figure(figsize=(20,16))
sns.heatmap(df.corr(), annot=True, cmap='YlGnBu')
plt.show()


# In[111]:


df6 = df.filter(items=['ID_CONTA','NU_DIAS_ATRASO','VALOR_CRELIQ','DIVIDA_ATUAL','QTD_PARCELAMENTO_3M','QTD_PARCELAMENTO_6M','QTD_PARCELAMENTO_12M','LIMITE','QTD_EXTRATOS','QTD_FX0_GERAL','QTD_FX1_GERAL', 'QTD_FX2_GERAL', 'QTD_FX0_3M', 'QTD_FX0_6M', 'QTD_FX1_3M', 'QTD_FX1_6M', 'QTD_FX2_3M', 'QTD_FX2_6M', 'QTD_CPC_10D', 'QTD_CPC_1M', 'QTD_CPC_3M', 'QTD_CPC_6M', 'QTD_CP_10D', 'QTD_CP_1M', 'QTD_CP_3M', 'QTD_CP_6M', 'QTD_ACIONAMENTO_10D', 'QTD_ACIONAMENTO_1M', 'QTD_ACIONAMENTO_3M','QTD_ACIONAMENTO_6M', 'RESPOSTA'])


# In[112]:


#Variável dependente e independente
x = df6.iloc[:, :-1]
y = df6.iloc[:, -1]


# In[113]:


x.head()


# In[114]:


y.head()


# In[115]:


#treino e teste
from sklearn.model_selection import train_test_split
x_train,x_test,y_train,y_test = train_test_split(x,y, test_size=0.25, random_state=0)


# In[116]:


print(x_train.shape)
print(x_test.shape)


# In[117]:


#Importando pacotes de avaliação
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report


# #### Modelo Preditivo ####

# In[124]:


#GradientBoostingClassifier:
from sklearn.ensemble import GradientBoostingClassifier
GradientBoost= GradientBoostingClassifier()
GradientBoost = GradientBoost.fit(x_train,y_train)


# In[125]:


#Predictions
y_pred = GradientBoost.predict(x_test)


# In[126]:


#Performance
print('Accuracy:', accuracy_score(y_test,y_pred))
print(confusion_matrix(y_test,y_pred))
print(classification_report(y_test,y_pred))


# #### FIM ####

--3.1 - Todas as compras realizadas no mes de janeiro de 2020 em lojas do estado do Ceara (CE)
SELECT 
    d_Pessoa.ID_PESSOA AS ID_da_pessoa,
    d_Pessoa.NM_PESSOA AS Nome_da_pessoa,
    d_Tempo.DT_REF AS Data_Referencia_da_Venda,
    f_Vendas.VL_VENDA AS Valor_da_Venda
FROM f_Vendas
LEFT JOIN d_Pessoa ON f_Vendas.ID_PESSOA = CAST(d_Pessoa.ID_PESSOA AS STRING)
LEFT JOIN d_Tempo ON f_Vendas.ID_TEMPO = CAST(d_Tempo.ID_TEMPO AS DATE)
LEFT JOIN d_Loja ON f_Vendas.ID_LOJA = CAST(d_Loja.ID_LOJA AS STRING)
WHERE
FORMAT_DATETIME('%Y-%m',d_Tempo.DT_REF) = '2020-01'
AND d_Loja.DS_UF = 'CE'



--3.2 - Quantidade de compras por cliente no mes de marco de 2020
SELECT 
    d_Pessoa.ID_PESSOA AS ID_da_pessoa,
    COUNT(DISTINCT f_Vendas.ID_VENDA) AS Quantidade_de_compras
FROM f_Vendas
LEFT JOIN d_Pessoa ON f_Vendas.ID_PESSOA = CAST(d_Pessoa.ID_PESSOA AS STRING)
LEFT JOIN d_Tempo ON f_Vendas.ID_TEMPO = CAST(d_Tempo.ID_TEMPO AS DATE)
LEFT JOIN d_Loja ON f_Vendas.ID_LOJA = CAST(d_Loja.ID_LOJA AS STRING)
WHERE
FORMAT_DATETIME('%Y-%m',d_Tempo.DT_REF) = '2020-03'
GROUP BY 1


--3.3 - Todos os clientes que nao fizeram compras no mes de marco de 2020
SELECT 
    d_Pessoa.ID_PESSOA AS ID_da_pessoa,
    d_Pessoa.NM_PESSOA AS Nome_da_pessoa
FROM f_Vendas
LEFT JOIN d_Pessoa ON f_Vendas.ID_PESSOA = CAST(d_Pessoa.ID_PESSOA AS STRING)
LEFT JOIN d_Tempo ON f_Vendas.ID_TEMPO = CAST(d_Tempo.ID_TEMPO AS DATE)
LEFT JOIN d_Loja ON f_Vendas.ID_LOJA = CAST(d_Loja.ID_LOJA AS STRING)
WHERE
FORMAT_DATETIME('%Y-%m',d_Tempo.DT_REF) <> '2020-03'



--3.4 - Data da ultima compra por cliente
SELECT 
    d_Pessoa.ID_PESSOA AS ID_da_pessoa,
    MAX(d_Tempo.DT_REF) AS Data_da_ultima_compra
FROM f_Vendas
LEFT JOIN d_Pessoa ON f_Vendas.ID_PESSOA = CAST(d_Pessoa.ID_PESSOA AS STRING)
LEFT JOIN d_Tempo ON f_Vendas.ID_TEMPO = CAST(d_Tempo.ID_TEMPO AS DATE)
LEFT JOIN d_Loja ON f_Vendas.ID_LOJA = CAST(d_Loja.ID_LOJA AS STRING)
GROUP BY 1